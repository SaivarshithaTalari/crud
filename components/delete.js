let mongodb = require("mongodb");
let client = mongodb.MongoClient;


let del = require("express").Router().delete("/:name", (req, res) => {
    client.connect("mongodb://localhost:27017/fsd55", (err, db) => {
        if (err) {
            res.status(500).json({ error: "Error connecting to the database" });
        }
        else {
            db.collection("participants").deleteOne({ "name": req.params.name }, (err, result) => {
                if (err) {
                    res.status(500).json({ error: "Error connecting to the database" });
                }
                else {
                    res.send(result);
                }
                db.close();
            });
        }
    });
});


module.exports = del;