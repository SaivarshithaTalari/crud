let express = require('express');
let bodyparser = require('body-parser');
let app = express();
const cros =require('cors');
app.use(cros());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:true}));
app.put('/',(req,res)=>{
    res.send("welcome home")
})


app.use('/register',require('./components/register'))
app.use('/login',require('./components/login'))
app.use('/update',require('./components/update'))
app.use('/fetch',require('./components/fetch'))
app.use('/delete',require('./components/delete'))




app.listen(4000,(req,res) =>{
    console.log('server running');
})
    
